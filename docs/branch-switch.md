# Switch OSTree branch

These instructions detail how to switch OSTree branches in a much
quicker manner than creating and burning a new SD card image.

You will need to have access to the robot's command line.

It is probably a good idea to stop the service which automatically
runs the script you will need to edit in case you don't want the robot
to reboot on its own after you have finished editing.
```
-sh-5.0# systemctl stop jetbot-upgrade.timer
```
This can be restarted afterwards if necessary by running the above
command with `start` in place of `stop`.

## Steps

1. Run `ostree admin unlock` to change the filesystem to development
   mode.

2. Using your favourite editor available on the robot, edit the file
   `/usr/lib/jetbot/jetbot-upgrade`, changing the line which reads
   `branch="<somebranch-name>"` to have the desired branch name between
   the quotes. (NB. forward slashes (`/`) in branch names should be
   converted to dashes (`-`) when used here. i.e. the branch
   `yourname/branch` should be written in this file as
   `yourname-branch`)

3. Save the file, your editor may complain that the file has been
   changed on the disk. If this is the case, writing for a second time
   should resolve this.

4. Run the file.
   ```
   -sh-5.0# /usr/lib/jetbot/jetbot-upgrade
   ```

If all goes according to plan the system will reboot itself into the
branch which was specified. This will happen automatically at the end of
the `jetbot-upgrade` script, so only run the script when you are ready
to reboot into the new branch.

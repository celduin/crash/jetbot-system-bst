# Jetbot System

## Introduction

This project aims to achieve a system able to be updated in a safe way via
atomic upgrades downloaded from a remote server, to achieve that, OSTree has
been implemented in top of an operative system. OSTree is an upgrade system for
Linux-based operating systems that performs atomic upgrades of complete
filesystem trees [0].

The operative system has been created from [Linux-tegra-4.9
kernel](https://github.com/madisongh/linux-tegra-4.9) and Freedesktop-SDK for
the basic filesystem. This supported by some Nvidia binaries result in a
functional OS for the Jetson Nano. Finally, BuildStream has been used to bake
all the ingredients.

## Jetbot's OSTree workflow Overview

When the system with OSTree in top of it is built, two elements will be created:

* The **repo** will be the sources that will be uploaded to a remote server.
* The **sysroot** will be the first rootfs that will be deployed in the Jetson
  Nano. A image will be created from it and it will be flashed into an SD card
  and introduced into the Jetson Nano.

Once the system is running, if changes are done to the OS during development,
this changes will produce some references and objects that will be uploaded to
the remote repo. The user from the Nano side, will:

* Pull those references and objects to the board.
* Execute a deployment command, which from the retrieved elements shall produce
  a new operative system that will be installed next to the old one. Both OS
  will be referenced in sysroot/boot/extlinux/extlinux.conf, by default the latest
  OS will be launched, however, in case of error, the old one shall be launched.
* Reboot the system, so the new OS gets launched.

# Getting started:

## Configuring the system

To have the system auto-connect to a wireless network and to pull upgrades
from your OSTree server, you can configure the system changing some
configuration variables.


To run the configuring script you need to set some environment variables.
More information about them in the [example configuration file](./config/example-vars)

    source config/example-vars
    sh config/template-files.sh

This script will replace some placeholders in some files consumed later
by BuildStream.

## Building the system repository

To build the system, you will need arm64 hardware. For the initial development
and integration of the system we used a Jetson TX1.

Additionally you will need extra disk space (and speed) for the builds.  In the
case of the Jetson TX1, you can  attach an SSD into it's SATA port.

Once you have the development environment ready, and have a copy of this
repository, you can run:

    bst build boards/minimal-systemd-nano-ostree-repo.bst

## Deployment

To deploy the system first we need to build an extra component that puts all
together to help us generate the SD card

    bst build boards/minimal-systemd-nano-ostree-sd.bst

Once that's built, you can get a checkout of the artifacts we have just built.

    bst checkout boards/minimal-systemd-nano-ostree-sd.bst deployment

Generate SD card image

    cd deployment
    ./create-sd-image.sh

Now you can flash the generated image into an SD card

    # Make sure you use the right device
    # sudo dd if=jetson-nano-sd-r32.1-2019-03-18.img of=/dev/yoursdcard bs=50M status=progress

## Automatic upgrades

The system includes some systemd services to automate the upgrades process. If
configured correctly (as mentioned in 'Configuring the system'), it will
check an OSTree repo to see if the branch that the current system is running
has changed. If it has, it will pull and deploy the new version.

To check the status of the sytem you can run:

    ostree admin status

If it says "pending" somewhere, that means that a new version was deployed, and
you still need to reboot de device to boot into the new version.

To check the status of the upgrades service you can:

    systemctl status jetbot-upgrade.service

or

    journalctl -u jetbot-upgrade.service


To see where are the upgrades coming from, or in other words,
what OSTree remotes you system has configured:

    ostree remote list -u

To disable the automatic upgrades service:

    systemctl disable jetbot-upgrade.timer
    systemctl stop jetbot-upgrade.timer

## Switching OSTree branches

It is possible to switch your OSTree branch on the robot without
creating and burning a new SD card image. This method is much faster and
detailed instructions on how to go about doing it are found
[here](./docs/branch-switch.md).

## Manual upgrade to a development version

**The default method of making changes and committing them is not going to be
used yet, instead follow the next process.**

Make a change in elements/boards/minimal-systemd-nano.bst to the rootfs, then
build the system with ostree in top of it:

    bst build boards/minimal-systemd-nano-ostree-repo.bst

Checkout the artifacts:

    bst checkout boards/minimal-systemd-nano-ostree-repo.bst ostree-repo

Expose ostree-repo/repo as server:

    cd ostree-repo/repo
    python -m SimpleHTTPServer 8000

Now, in the Jetson Nano you can pull the changes from the repository and
deploy them. First of all disable the `jetbot-upgrade` timer:

    systemctl disable jetbot-upgrade.timer
    systemctl stop jetbot-upgrade.timer

Configure a remote pointing to your development device:

    ostree remote add --no-gpg-verify my-dev-board http://192.168.1.101:8080

Pull and deploy the upgrade

    ostree pull my-dev-board
    ostree admin deploy --os=ct-os my-dev-board:jetbot-nano

# References

[0] OSTree Overview: https://ostree.readthedocs.io/en/latest/manual/introduction/

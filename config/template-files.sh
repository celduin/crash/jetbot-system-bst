sed -e "s/WIFISSID/$WIFI_SSID/g" -i files/wifi/wpa_supplicant-wlan0.conf
sed -e "s/WIFIPASS/$WIFI_PASS/g" -i files/wifi/wpa_supplicant-wlan0.conf
sed -e "s#OSTREEREMOTEURL#$OSTREE_URL#g" -i elements/boards/minimal-systemd-nano-ostree-sd.bst
# replace slashes with dashes
OSTREE_BRANCH_CLEAN=${OSTREE_BRANCH//\//-}
sed -e "s#TRACKEDBRANCH#$OSTREE_BRANCH_CLEAN#g" -i elements/boards/minimal-systemd-nano-ostree-repo.bst
sed -e "s#TRACKEDBRANCH#$OSTREE_BRANCH_CLEAN#g" -i elements/boards/minimal-systemd-nano-ostree-sd.bst
sed -e "s#TRACKEDBRANCH#$OSTREE_BRANCH_CLEAN#g" -i files/upgrade-scripts/jetbot-upgrade
